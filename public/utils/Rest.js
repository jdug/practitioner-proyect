(function (window) {
    'use strict';
    function Rest(url, body) {
        this.location = location;
        this.url = url;
        this.body = body;
    }

    Rest.prototype.getRestUrlPrefix = function () {
        return '/api/rest/';
    }

    Rest.prototype.get = function (url, body, token) {
        return fetch(url, {
            method: 'GET',
            body: JSON.stringify(body),
            credentials: "same-origin",
            headers: {
                'Accept': 'image/*, application/json',
                'Content-Type': 'application/json',
                'Authorization': token || ''
            }
        }).then(function (res) {
            return res;
        });
    }

    Rest.prototype.post = function (url, body, token) {
        return fetch(url, {
            method: 'POST',
            body: JSON.stringify(body),
            credentials: "same-origin",
            headers: {
                'Accept': 'image/*, application/json',
                'Content-Type': 'application/json',
                'Authorization': token || ''
            }
        }).then(function (res) {
            return res;
        });
    }

    Rest.prototype.put = function (url, body, token) {
        return fetch(url, {
            method: 'PUT',
            body: JSON.stringify(body),
            credentials: "same-origin",
            headers: {
                'Accept': 'image/*, application/json',
                'Content-Type': 'application/json',
                'Authorization': token || ''
            }
        }).then(function (res) {
            return res;
        });
    }

    Rest.prototype.delete = function (url, body, token) {
        return fetch(url, {
            method: 'DELETE',
            body: JSON.stringify(body),
            credentials: "same-origin",
            headers: {
                'Accept': 'image/*, application/json',
                'Content-Type': 'application/json',
                'Authorization': token || ''
            }
        }).then(function (res) {
            return res;
        });
    }

    // Export to window
    window.utils = window.utils || {};
    window.utils.Rest = Rest;
})(window);

// var test = new utils.Rest();
// var data = {test: 'TEST', number: 1};
// var token = 'a@1D3g^@';
// test.post('http://httpbin.org/post', data, token).then(function(res) {
//     console.log('response', res);
// });