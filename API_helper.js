//const request = require('request')

// load the things we need
var express = require('express');
var request = require('request');
const promise = require('bluebird');
var app = express();



module.exports = {
	make_API_call : function(url){
		return new Promise((resolve, reject) => {
			request(url, function (error, response, body) {
				if (!error && response.statusCode == 200) {
					var obj = JSON.parse(body);
					var keysArray = Object.keys(obj);
					//Aquí obtenemos los tipos de cambio en formato JSON.
					var tc = obj[(keysArray[0])];
					console.log(tc);
					//Aquí especificamos las llaves, una es "compra" y la otra "venta".
					var tc_llave = Object.keys(tc);
					console.log(tc_llave);
					//Recuperamos los valores que tienen la compra y la venta respectivamente.
					var compra = tc[(tc_llave[0])];
					var venta  = tc[(tc_llave[1])];
					console.log(compra);
					console.log(venta);
					next();
				}
			});
		})
	}
}