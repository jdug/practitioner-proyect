'use strict';

const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');
const User = require('../models/user');
const api_helper = require('../API_helper');
const request = require('request');
// Registro de Usuario:       ************************************************************
router.post('/register', (req, res, next) => {
  let newUser = new User ({
    name: req.body.name,
    email: req.body.email,
    username: req.body.username,
    password: req.body.password
  });

  User.addUser(newUser, (err, user) => {
    if(err) {
      res.json({success: false, msg: 'Falló el registro del usuario'});
    } else {
      res.json({success: true, msg: 'Usuario Registrado'});
    }
  });
});

// Validación de Usuario:     ************************************************************
// Realizamos la validación del usuario mediante su username y contraseña...
router.post('/authenticate', (req, res, next) => {
  const username = req.body.username;
  const password = req.body.password;

  User.getUserByUsername(username, (err, user) => {
    if(err) throw err;
    if(!user) {
      return res.status(401).send({ error: 'Inicio de sesión fallida...' });
    }

    User.comparePassword(password, user.password, (err, isMatch) => {
      if(err) throw err;
      if(isMatch) {
        const token = jwt.sign({data: user}, config.secret, {
          expiresIn: 604800 // 1 week
        });
        /////
        let direccion = '2019-05-16';
        //let url = `https://api.sunat.cloud/cambio/${direccion}`;
        let url = 'https://api.sunat.cloud/cambio/'+direccion;
        //api_helper.make_API_call(url)
        /////
        request(url, function (error, response, body) {
          if (!error && response.statusCode == 200) {
            var obj = JSON.parse(body);
            var keysArray = Object.keys(obj);
            //Aquí obtenemos los tipos de cambio en formato JSON.
            var tc = obj[(keysArray[0])];
            //console.log(tc);
            //Aquí especificamos las llaves, una es "compra" y la otra "venta".
            var tc_llave = Object.keys(tc);
            //console.log(tc_llave);
            //Recuperamos los valores que tienen la compra y la venta respectivamente.
            var compra = tc[(tc_llave[0])];
            var venta  = tc[(tc_llave[1])];
            console.log(compra);
            console.log(venta);
            //console.log(storedUser.compra);
            res.json({
              success: true,
              token: 'JWT '+token,
              user: {
                id: user._id,
                name: user.name,
                username: user.username,
                email: user.email
              },
              compra: compra,
              venta: venta
            })
          }
        });
      } else {
        return res.status(401).send({ error: 'Sesión fallida.' });
      }
    });
  });
});

// Profile                      ************************************************************
router.get('/profile', passport.authenticate('jwt', {session:false}), (req, res, next) => {
  res.json({user: req.user});
});

module.exports = router;
