const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const passport = require('passport');
const path = require('path');
const argv = require('yargs').argv;
const cors = require('cors');
const config = require('./config/database');
const app = express();
const api_helper = require('./API_helper');
const request = require('request');
// DB Config
const db = require('./config/database').database;
// Port Number
const port = process.env.PORT || 3000;

////////////////////////
// Connect to MongoDB //
////////////////////////
mongoose
  .connect(db, { useNewUrlParser: true })
  .then(() => console.log('MongoDB Connected'))
  .catch(err => console.log(err));

/////////////////////////
// Definición de Rutas //
/////////////////////////
const users = require('./routes/users');

// CORS Middleware
app.use(cors());

// Set Static Folder
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'utils')));

// Body Parser Middleware
// Soporte para body's codificados  en jsonsupport.
app.use(bodyParser.json());

// Passport Middleware
app.use(passport.initialize());
app.use(passport.session());

require('./config/passport')(passport);

app.use('/users', users);

///////////
// Rutas //
///////////
app.get('/', (req, res) => {
  res.send('invaild endpoint');
});

//En caso no cumpla con ninguna llamada, proceder con presentar el index.html
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'public/index.html'));
});

// Start Server
app.listen(port, () => {
  console.log('Server started on port '+port);
});

