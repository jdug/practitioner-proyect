
##### Prerequisites:

    node
    npm
    bower
    mongodb
    express
    jwt

##### Setup:

    npm install
    bower install

### Inicio en Modo Desarrollo (Con nodemon y Inspect)

    npm run dev

### Inicio en versión productiva:

    npm start

### Navigate to
    localhost:3000/